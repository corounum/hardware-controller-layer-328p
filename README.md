# Controller Layer (328p)

The controller layer of the sensor stack is based on the [bare board Arduino design](https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard).
Our insperation for this controller came from our limited design restraints and various reputible sources that attempted
similar projects before us. 

This board contains the ATMEGA 328p TQFP processor and the DS3231 real-time clock. There is a FTDI 6-pin header along with
an 8-pin header that allows our boards to communicate through the serial clock (SCL) and serial data (SDA) lines. 
We utilize the 8MHz internal oscillator within the processor so that our final board no longer needs an external crystal.
Using a 32-pin adapter and a small breadboard we were able to successfully burn the bootloader to obtain our desired 
processor settings. We recommend following [this tutorial](https://github.com/MCUdude/MiniCore) as it was the only procedure that 
successfully burned the bootloader onto our processor. 
